# Docker Image for PHP/Node/Laravel Apps

NOTE: THIS IS A PUBLIC REPOSITORY.

# Related Repositories

(https://hub.docker.com/r/emediaorg/php)

# Build instructions

Step 1. To build a new image with a tag, run this locally. Change last numbers to the current tag.

```bash
docker build --platform linux/amd64 --tag emediaorg/php:8.3-202407 .
```

Step 2. Login to your Docker account

```bash
docker login
```

Step 3. Then push the updated image to Docker Hub.

```bash
docker push emediaorg/php:8.3-202407
```

## Version History

| Tag        | Description                  |
|------------|------------------------------|
| 0.0.1      | PHP 7.4, Node 12.x           |
| 8.1.0      | PHP 8.1, Node 17.x           |
| 8.1-202303 | PHP 8.1, Node 19.x, PCov     |
| 8.2-202407 | PHP 8.2, Node 20.x LTS, PCov |
| 8.3-202407 | PHP 8.3, Node 20.x LTS, PCov |
